import {testMiddleware} from '@dkx/http-server';
import {expect} from 'chai';
import {conditionMiddleware} from '../../lib';


describe('#conditionMiddleware', () => {

	it('should process next middleware', async () => {
		let called: boolean = false;

		await testMiddleware(conditionMiddleware(async () => {
			return true;
		}), {
			next: async (res) => {
				called = true;
				return res;
			},
		});

		expect(called).to.be.equal(true);
	});

	it('should not process next middleware', async () => {
		let called: boolean = false;

		await testMiddleware(conditionMiddleware(async () => {
			return false;
		}), {
			next: async (res) => {
				called = true;
				return res;
			},
		});

		expect(called).to.be.equal(false);
	});

});
