import {Middleware, Request, Response, NextMiddlewareFunction, RequestState} from '@dkx/http-server';


export function conditionMiddleware(condition: (req: Request, res: Response, state: RequestState) => Promise<boolean>): Middleware
{
	return async (req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response> =>
	{
		const result = await condition(req, res, state);

		if (result) {
			return next(res);
		}

		return res;
	}
}
