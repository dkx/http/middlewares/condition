# DKX/Http/Middleware/Condition

Condition middleware for [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Installation

```bash
$ npm install --save @dkx/http-middleware-condition
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-condition
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {conditionMiddleware} = require('@dkx/http-middleware-condition');

const app = new Server;

app.use(conditionMiddleware(async function(request, response, state) {
	return true;
}));
```

If `false` is returned, no further middleware will be called.
